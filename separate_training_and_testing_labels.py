import os
import rasterio
import numpy as np
import pandas as pd
import geopandas as gpd
from shapely.wkt import dumps, loads
import shapely
import subprocess
from tqdm import tqdm
from build_labels_in_kitti_format import create_directory_if_does_not_exist
import shutil
import random
from scipy import misc
import sys
from skimage import transform,io




def randomly_assign_image_labels_to_training_and_test(labels_source, images_source, log_directory, percentage_train, new_root_directory, empty_image_threshold):

    percentage_test = 1.0 - percentage_train
    # get list of files in directory
    filesdict = get_set_of_files_in_directory(labels_source, 'txt')
    df_labels_img = pd.DataFrame(list(filesdict.items()),
                 columns=['label_id', 'label_original_path'])

    # add random column to dataframe
    random.seed(4)
    df_labels_img['random_number'] = 0
    df_labels_img.loc[:,'random_number'] = np.random.uniform(0., 1., df_labels_img.shape[0])

    # test that image files are valid
    df_labels_img['file_is_valid'] = 0
    print('validate image paths and write to log file')
    for i,row in tqdm(df_labels_img.iterrows()):
        temp_filename = os.path.join(images_source, df_labels_img.loc[i,'label_id'] + '.png')
        res = validate_file_exists(temp_filename)
        if not res:
            df_labels_img.loc[i, 'image_original_path'] = 0
        elif res:
            df_labels_img.loc[i, 'image_original_path'] = res
            df_labels_img.loc[i, 'file_is_valid'] = 1

    print('validate that images are not mostly empty/cropped pixels')
    df_labels_img.loc[:,'above_black_pixel_threshold'] = 0
    for i, row in tqdm(df_labels_img.iterrows()):
        res = check_percentage_empty_pixels(df_labels_img.loc[i, 'image_original_path'], empty_image_threshold)
        if res is True:
            df_labels_img.loc[i, 'above_black_pixel_threshold'] = 1
            df_labels_img.loc[i, 'file_is_valid'] = 1
        elif res is False:
            df_labels_img.loc[i, 'file_is_valid'] = 0

    df_labels_img.loc[df_labels_img['random_number'] <= percentage_train , 'Group'] = 'Train'
    df_labels_img.loc[df_labels_img['random_number'] > percentage_train , 'Group'] = 'Test'
    # create training and testing directories.
    new_training_image_directory, new_training_labels_directory, new_test_image_directory,new_test_labels_directory = create_new_train_test_directories(new_root_directory)

    # assign files to new directory locations.
    df_labels_img['new_label_path'] = 0
    df_labels_img['new_image_path'] = 0

    print('creating directory assignments for valid files')
    for i,row in df_labels_img.iterrows():
        if df_labels_img.loc[i,'Group'] == 'Train' and df_labels_img.loc[i,'file_is_valid'] == 1:
            df_labels_img.loc[i,'new_label_path'] = new_training_labels_directory
            df_labels_img.loc[i,'new_image_path'] = new_training_image_directory
        if df_labels_img.loc[i,'Group'] == 'Test' and df_labels_img.loc[i,'file_is_valid'] == 1:
            df_labels_img.loc[i,'new_label_path'] = new_test_labels_directory
            df_labels_img.loc[i,'new_image_path'] = new_test_image_directory

    # move files to new directories
    print('moving files to new directories')
    for i,row in tqdm(df_labels_img.iterrows()):
        if df_labels_img.loc[i, 'file_is_valid'] == 1:
            shutil.move(df_labels_img.loc[i,'image_original_path'], df_labels_img.loc[i,'new_image_path'])
            shutil.move(df_labels_img.loc[i,'label_original_path'], df_labels_img.loc[i,'new_label_path'])

    path_log_file = os.path.join(log_directory, 'preprocessing_log.csv')
    df_labels_img.to_csv(path_log_file)


def get_image_path(row):
    return row


def separate_training_and_testing_labels(labels_source_directory, training_images_directory, testing_images_directory):

    # create new directories for training and testing labels
    create_directory_if_does_not_exist(os.path.join(training_images_directory, 'training_labels/'))
    create_directory_if_does_not_exist(os.path.join(testing_images_directory, 'testing_labels/'))
    training_labels_directory = os.path.join(training_images_directory,'training_labels/')
    testing_labels_directory = os.path.join(testing_images_directory, 'testing_labels/')
    # get list of files in the directories.
    print('loading images into memory')
    dict_new_label_files = get_set_of_files_in_directory(labels_source_directory, 'txt')
    dict_training_image_files = get_set_of_files_in_directory(training_images_directory, 'tif')
    dict_testing_image_files = get_set_of_files_in_directory(testing_images_directory, 'tif')
    # get list of label files to move to training
    print('moving label files to target directories.')
    move_files_to_new_directory(dict_new_label_files, dict_training_image_files, training_labels_directory)
    move_files_to_new_directory(dict_new_label_files, dict_testing_image_files, testing_labels_directory)



def get_set_of_files_in_directory(directoryname, extension):
    filelist = []
    for file in os.listdir(directoryname):
        if file.endswith(extension):
            filelist.append(os.path.join(directoryname,file))
    return dict(zip(get_image_name_from_path(filelist), filelist))

def get_image_name_from_path(filelist):
    return list(map(lambda x: os.path.splitext(os.path.basename(x))[0], filelist))

def move_files_to_new_directory(dict_all_files, dict_compare, target_directory):
    set_all = set(dict_all_files.keys())
    set_compare = set(dict_compare.keys())
    files_to_move = set_all.intersection(set_compare)
    for f in tqdm(files_to_move):
        newpath = os.path.join(target_directory, f+'.txt')
        shutil.move(dict_all_files[f], newpath)


def check_percentage_empty_pixels(image_path, empty_threshold):
    img = io.imread(image_path, as_grey=True)
    img_flat = img.flatten()
    black = np.sum(img_flat == 0)
    all = img_flat.size
    percentage_black = black/all
    if percentage_black <= empty_threshold:
        return 1
    else:
        return 0

def create_new_train_test_directories(new_root_directory):

    new_training_root = os.path.join(new_root_directory, 'training/')
    new_training_image_directory = os.path.join(new_root_directory, 'training/images/')
    new_training_labels_directory = os.path.join(new_root_directory, 'training/labels/')
    new_test_root = os.path.join(new_root_directory, 'test/')
    new_test_image_directory = os.path.join(new_root_directory, 'test/images/')
    new_test_labels_directory = os.path.join(new_root_directory, 'test/labels/')

    new_directories = [new_training_root,
                       new_training_image_directory,
                       new_training_labels_directory,
                       new_test_root,
                       new_test_image_directory,
                       new_test_labels_directory]
    for p in new_directories:
        if not os.path.exists(p):
            os.makedirs(p)

    return (new_training_image_directory,
            new_training_labels_directory,
            new_test_image_directory,
            new_test_labels_directory)

def validate_file_exists(path):
    if os.path.exists(path):
        return path
    else:
        return 0


if __name__ == "__main__":
    randomly_assign_image_labels_to_training_and_test('/media/hayagriva/data/spacenet/AOI_2_Vegas_Train/RGB-PanSharpen/new_labels/',
  '/media/hayagriva/data/spacenet/AOI_2_Vegas_Train/RGB-PanSharpen/8bit_png_output/',
  '/media/hayagriva/data/spacenet/AOI_2_Vegas_Train/log/',
  0.80,
  '/home/krishnab/Downloads/lasvegaspix/')