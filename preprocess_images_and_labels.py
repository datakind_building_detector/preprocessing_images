import os
from os import walk
import rasterio
import numpy as np
import pandas as pd
import geopandas as gpd
from shapely.wkt import dumps, loads
from osgeo import gdal

import subprocess
from tqdm import tqdm
from build_labels_in_kitti_format import create_directory_if_does_not_exist
from build_labels_in_kitti_format import process_raw_spacenet_labels_to_kitti_format
from separate_training_and_testing_labels import randomly_assign_image_labels_to_training_and_test
import shutil
import random


def preprocess_images_and_labels(root_directory, percentage_train, empty_image_threshold):

    # check directory to make sure it contains images and labels.
    root_directory, image_directory, labels_directory = check_if_image_and_labels_directories_exist(root_directory)
    # clean directories from previous runs.
    clean_directories_for_processing(root_directory, image_directory, labels_directory)

    # convert image files to 8bit format.
    png_images_directory = convert_resize_image_files_to_8bit(image_directory, 1280, 1280)

    # convert labels into txt files. This will also remove any labels that contain null geometries.
    labels_target_directory = os.path.join(root_directory, 'kitti_formatted_labels/')
    labels_file = list_files(os.path.join(root_directory, 'summaryData/'), 'csv')
    process_raw_spacenet_labels_to_kitti_format(labels_file[0],
                                                labels_target_directory,
                                                png_images_directory)

    log_directory = os.path.join(root_directory, 'preprocessing_log_file/')
    create_directory_if_does_not_exist(log_directory)

    randomly_assign_image_labels_to_training_and_test(labels_target_directory,
                                                      png_images_directory,
                                                      log_directory,
                                                      percentage_train,
                                                      root_directory,
                                                      empty_image_threshold)

    print('preprocessing complete')



def check_if_image_and_labels_directories_exist(directory):
    image_directory = os.path.join(directory,'RGB-PanSharpen/')
    labels_directory = os.path.join(directory,'summaryData/' )
    if not os.path.exists(image_directory):
        print('invalid directory. No image directory found.')
        raise
    if not os.path.exists(labels_directory):
        print('invalid directory. No labels directory found.')
        raise

    print('image and labels directories found. Directory validated.')

    return directory, image_directory, labels_directory

def clean_directories_for_processing(root_directory, image_directory, labels_directory):

    if os.path.exists(os.path.join(root_directory, 'kitti_formatted_labels/')):
        shutil.rmtree(os.path.join(root_directory, 'kitti_formatted_labels/'))
    if os.path.exists(os.path.join(root_directory, 'training/')):
        shutil.rmtree(os.path.join(root_directory, 'training/'))
    if os.path.exists(os.path.join(root_directory, 'test/')):
        shutil.rmtree(os.path.join(root_directory, 'test/'))
    if os.path.exists(os.path.join(root_directory, 'preprocessing_log_file/')):
        shutil.rmtree(os.path.join(root_directory, 'preprocessing_log_file/'))
    if os.path.exists(os.path.join(image_directory, '8bit_png_output/')):
        shutil.rmtree(os.path.join(image_directory, '8bit_png_output/'))
    print('directory cleaned for preprocessing.')

def convert_image_files_to_8bit(image_directory):

    # test if the image directories are already created, otherwise create and process them.
    # converting the images takes a long time, so no need to repeat this step over and over.
    if not os.path.exists(os.path.join(image_directory, '8bit_png_output/')):
        subprocess.call(['./convert_images_8bit', image_directory])

    converted_image_directory = os.path.join(image_directory, '8bit_png_output/')
    # remove extraneous xml files from converted image directory.
    delete_gdal_xml_files(converted_image_directory)

    return converted_image_directory


def convert_resize_image_files_to_8bit(image_directory, xsize, ysize):
    converted_image_directory = os.path.join(image_directory, '8bit_png_output/')
    images = list_files(image_directory, 'tif') # these are full file paths
    outsize = f'-outsize {xsize} {ysize}'
    options_list = [
        '-ot Byte',
        '-of PNG',
        '-scale',
        outsize
    ]
    options_string = " ".join(options_list)

    if not os.path.exists(converted_image_directory):
        print('converting images to PNG format and resizing.')
        os.mkdir(converted_image_directory)
        for img in tqdm(images):
            new_filename = get_image_name_from_path(img, converted_image_directory, 'png')
            gdal.Translate(new_filename,
                           img,
                           options=options_string)

    delete_gdal_xml_files(converted_image_directory)
    return converted_image_directory

def get_image_name_from_path(file, target_directory, extension):
    file_stem = os.path.splitext(os.path.basename(file))[0]
    new_filename = file_stem + '.'+ extension
    return os.path.join(target_directory, new_filename)

def delete_gdal_xml_files(image_directory):
    files = list_files(image_directory, 'aux.xml')
    for f in tqdm(files):
        os.remove(f)

def delete_file_if_exists(filename):
    if os.path.exists(filename):
        os.remove(filename)

def list_files(directory, extension):
    file_list = []
    for (dirpath, dirnames, filenames) in walk(directory):
        for f in filenames:
            if f.endswith('.' + extension):
                file_list.append(os.path.join(dirpath, f))
    return file_list





if __name__ == "__main__":
    preprocess_images_and_labels('/media/hayagriva/data/spacenet/AOI_2_Vegas_Train/',
                                 0.90,
                                 0.50)
