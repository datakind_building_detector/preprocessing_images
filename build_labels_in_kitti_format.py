
import os
import rasterio
import numpy as np
import pandas as pd
import geopandas as gpd
from shapely.wkt import dumps, loads
import shapely
import subprocess
from tqdm import tqdm

kitti_columns = ['ImageId',
                 'type',
                 'truncated',
                 'occluded',
                 'alpha',
                 'bbox_left_minx',
                 'bbox_top_miny',
                 'bbox_right_maxx',
                 'bbox_bottom_maxy',
                 'dimension_height',
                 'dimension_width',
                 'dimension_length',
                 'location_x',
                 'location_y',
                 'location_z',
                 'rotation_y']

box_coordinate_column_match = {'minx':'bbox_left_minx',
                            'miny':'bbox_top_miny',
                            'maxx':'bbox_right_maxx',
                            'maxy':'bbox_bottom_maxy'}

def process_raw_spacenet_labels_to_kitti_format(label_file, labels_target_directory, image_source_directory):
    # first load spacenet data
    create_directory_if_does_not_exist(labels_target_directory)
    print('Processing csv data')
    gdf = create_geodataframe_from_csv(label_file)
    print('Removing invalid data such as zero-sized geometries or pictures with no buildings.')
    gdf_validated, gdf_for_removal = validate_geodataframe_to_remove_null_geometries(gdf)
    print('Creating bounding boxes for labeled geometries.')
    kitti_data = extract_bounding_boxes_from_labeled_geometries2(gdf_validated)
    print('Write text files with new image labels.')
    generate_txt_label_files(kitti_data, labels_target_directory)
    remove_image_files_with_null_geometries(gdf_validated, image_source_directory)
    delete_file_if_exists(os.path.join(labels_target_directory, 'nan.txt'))

def create_geodataframe_from_csv(label_file):
    csv_labels = pd.read_csv(label_file)
    geometry = csv_labels['PolygonWKT_Pix'].map(loads)
    crs = {'init': 'epsg:4326'}
    gdf = gpd.GeoDataFrame(csv_labels, crs=crs, geometry=geometry)
    return gdf

def validate_geodataframe_to_remove_null_geometries(gdf):
    gdf_valid = gdf.loc[gdf.BuildingId >= 0, :]
    gdf_valid = gdf_valid.loc[gdf_valid.geometry.area > 0, :]
    gdf_null_geometries = gdf.loc[gdf.BuildingId < 0, :]
    return (gdf_valid, gdf_null_geometries)

def extract_bounding_boxes_from_labeled_geometries2(source_df):
    _, kitti_data = create_kitti_label_empty_dataframe(source_df.shape[0])
    kitti_data.loc[:, 'ImageId'] = source_df.loc[:,'ImageId']
    kitti_data.loc[:, 'ImageId'] = 'RGB-PanSharpen_' + kitti_data.loc[:, 'ImageId']
    kitti_data.drop(['bbox_left_minx', 'bbox_top_miny', 'bbox_right_maxx', 'bbox_bottom_maxy'], axis=1, inplace=True)
    boxes = source_df.geometry.envelope.scale(1280/650, 1280/650, origin=(0, 0)).bounds
    boxes.columns = ['bbox_left_minx', 'bbox_top_miny', 'bbox_right_maxx', 'bbox_bottom_maxy']
    print('boxes is of type: ' + str(type(boxes)))
    new_data = pd.concat([kitti_data, boxes], axis=1)

    return new_data[kitti_columns]


def generate_txt_label_files(source_df, labels_target_directory):
    # create new files for each valid image
    kitti_columns_csv = ['truncated',
                     'occluded',
                     'alpha',
                     'dimension_height',
                     'dimension_width',
                     'dimension_length',
                     'location_x',
                     'location_y',
                     'location_z',
                     'rotation_y']
    valid_image_list = source_df.ImageId.unique()
    list_of_output_columns = source_df.columns.tolist()
    list_of_output_columns.remove('ImageId')
    for im in tqdm(valid_image_list):
        filename = labels_target_directory + str(im) + '.txt'
        tempdf = source_df.loc[source_df.ImageId == im, list_of_output_columns]
        tempdf[kitti_columns_csv] = tempdf[kitti_columns_csv].astype('int64')
        tempdf.to_csv(filename, sep=" ", header=False,index=False)


def create_kitti_label_empty_dataframe(rows):
    kitti_formatted_data = pd.DataFrame(np.zeros((rows, len(kitti_columns))), columns=kitti_columns)
    kitti_formatted_data['type'] = 'building'
    kitti_formatted_data['truncated'] = 0
    kitti_formatted_data['occluded'] = 0
    return(kitti_formatted_data.shape, kitti_formatted_data)

def remove_image_files_with_null_geometries(df, image_source_directory):
    list_of_files_for_removal = df.ImageId.unique()
    for f in tqdm(list_of_files_for_removal):
        filename1 = image_source_directory + str(f) + '.png'
        filename2 = image_source_directory + str(f) + '.jpg'
        if os.path.exists(filename1):
            os.remove(filename1)
            print('removed file:', filename)
        if os.path.exists(filename2):
            os.remove(filename2)
            print('removed file:', filename)

def create_directory_if_does_not_exist(directoryname):
    if not os.path.exists(directoryname):
        os.makedirs(directoryname)
        print('created directory: ' + str(directoryname))

def delete_file_if_exists(filename):
    if os.path.exists(filename):
        os.remove(filename)

if __name__ == "__main__":
    process_raw_spacenet_labels_to_kitti_format('/media/hayagriva/data/spacenet/AOI_2_Vegas_Train/summaryData/AOI_2_Vegas_Train_Building_Solutions.csv',
         '/media/hayagriva/data/spacenet/AOI_2_Vegas_Train/RGB-PanSharpen/new_labels/',
         '/media/hayagriva/data/spacenet/AOI_2_Vegas_Train/RGB-PanSharpen/')